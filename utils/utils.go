package utils

import (
	"bytes"
	"encoding/json"
	"hash/crc64"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"plugin"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func HashByteToUint64(data []byte) uint64 {
	t := crc64.MakeTable(37)
	return crc64.Checksum(data, t)
}

func ModularByte(data []byte, mod uint64) uint64 {
	return HashByteToUint64(data) % mod
}

func ToJSON(data []byte) (map[string]interface{}, error) {
	var jobj map[string]interface{}
	err := json.Unmarshal(data, &jobj)
	if err != nil {
		return nil, err
	}
	return jobj, nil
}

func VerifyRequires(params map[string]string, requires []string) error {
	for _, require := range requires {
		if val, ok := params[require]; !ok || val == "" {
			return errors.Errorf("invalid params, missing key=%s", require)
		}
	}
	return nil
}

func ExecCmd(cmdName string, args ...string) (string, string, error) {
	// return stdout, stderr, error
	cmd := exec.Command(cmdName, args...)
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	return stdout.String(), stderr.String(), err
}

func FileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func ParseInteger(intStr string, defaultValue int) int {
	res, err := strconv.Atoi(intStr)
	if err != nil || res < 0 {
		return defaultValue
	}
	return res
}

func RstripSpace(buf []byte) []byte {
	// Strip away trailing space
	if buf[len(buf)-1] != ' ' {
		return buf
	}

	for i := len(buf) - 1; i >= 1; i-- {
		if buf[i] != ' ' {
			return buf[0 : i+1]
		}
	}

	if buf[0] != ' ' {
		return buf[0:1]
	}
	return nil
}

func LoadPlugins(path string, funcName string, skipError bool) (map[string]plugin.Symbol, error) {
	var matches []string
	if !strings.HasSuffix(path, ".so") {
		path = filepath.Join(path, "*.so")

		var err error
		if matches, err = filepath.Glob(path); err != nil {
			return nil, errors.Wrap(err, "Failed to find plugins")
		}
	} else {
		matches = append(matches, path)
	}

	plugins := make(map[string]plugin.Symbol)
	for _, f := range matches {
		p, err := plugin.Open(f)
		if err != nil {
			if skipError {
				continue
			} else {
				return nil, errors.Wrap(err, "Failed to load plugin")
			}
		}

		sym, err := p.Lookup(funcName)
		if err != nil {
			if skipError {
				continue
			} else {
				return nil, errors.Wrap(err, "Failed to load plugin")
			}
		}
		plugins[f] = sym
	}
	return plugins, nil
}

func NewLogger(logLevel string, prod bool) (*zap.Logger, error) {
	var config zap.Config
	if prod {
		config = zap.NewDevelopmentConfig()
	} else {
		config = zap.NewProductionConfig()
	}

	level := zapcore.InfoLevel
	switch logLevel {
	case "DEBUG":
		level = zapcore.DebugLevel
	case "INFO":
		level = zapcore.InfoLevel
	case "WARN":
		level = zapcore.WarnLevel
	case "ERROR":
		level = zapcore.ErrorLevel
	}
	config.Level = zap.NewAtomicLevelAt(level)

	logger, err := config.Build()
	return logger, err
}

func WaitForSignals(callback func(os.Signal), sigs ...os.Signal) {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, sigs...)

	sig := <-sigChan
	if callback != nil {
		callback(sig)
	}
}
