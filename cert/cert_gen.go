package cert

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"net"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
)

type Config struct {
	Host string
	Org  string

	// Date time in RFC3339 format (a.k.a "2006-01-02T15:04:05Z07:00")
	ValidFrom string

	ValidFor   time.Duration
	IsCA       bool
	RsaBits    int
	EcdsaCurve string
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) (*pem.Block, error) {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}, nil
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			return nil, errors.Wrap(err, "unable to marshal ECDSA private key")
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}, nil
	default:
		return nil, errors.Errorf("unsupported type %+v", priv)
	}
}

type Certificate struct {
	Cert []byte
	Key  []byte
}

func gen(c *Config, share int, certsChan chan []*Certificate, errChan chan error, wg *sync.WaitGroup, id int) {
	defer wg.Done()

	var certs []*Certificate
	for i := 0; i < share; i++ {
		select {
		case err := <-errChan:
			// there is error found, relay the err
			errChan <- err
			return
		default:
		}

		cert, err := GenerateCert(c)
		if err != nil {
			errChan <- err
			return
		}
		certs = append(certs, cert)
	}
	certsChan <- certs
}

// GenerateCert generates cert, and private key and returns (cert, private key, error)
func GenerateCert(c *Config) (*Certificate, error) {
	var priv interface{}
	var err error
	switch c.EcdsaCurve {
	case "":
		priv, err = rsa.GenerateKey(rand.Reader, c.RsaBits)
	case "P224":
		priv, err = ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	case "P256":
		priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	case "P384":
		priv, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	case "P521":
		priv, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	default:
		return nil, errors.Errorf("unrecognized elliptic curve: %q", c.EcdsaCurve)
	}

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate private key")
	}

	var notBefore time.Time
	if len(c.ValidFrom) == 0 {
		notBefore = time.Now()
	} else {
		notBefore, err = time.Parse(time.RFC3339, c.ValidFrom)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to parse creation date %s", c.ValidFrom)
		}
	}

	notAfter := notBefore.Add(c.ValidFor)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate serial number")
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{c.Org},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:    x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},

		BasicConstraintsValid: true,
	}

	hosts := strings.Split(c.Host, ",")
	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}

	if c.IsCA {
		template.IsCA = true
		template.KeyUsage |= x509.KeyUsageCertSign
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create certificate")
	}

	cert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	block, err := pemBlockForKey(priv)
	if err != nil {
		return nil, err
	}

	key := pem.EncodeToMemory(block)
	return &Certificate{Cert: cert, Key: key}, nil
}

// GenerateCerts use all CPUs in the host to generate numberCerts concurrently
func GenerateCerts(c *Config, numberCerts int) ([]*Certificate, error) {
	cpus := runtime.NumCPU()
	share := numberCerts / cpus
	lastShare := numberCerts/cpus + numberCerts%cpus
	certsChan := make(chan []*Certificate, cpus)
	errChan := make(chan error, cpus)

	var wg sync.WaitGroup
	for i := 0; i < cpus-1; i++ {
		wg.Add(1)
		go gen(c, share, certsChan, errChan, &wg, i+1)
	}
	wg.Add(1)
	go gen(c, lastShare, certsChan, errChan, &wg, cpus)

	var allCerts []*Certificate
	done := make(chan struct{})

	// results collector
	var firstErr error
	go func() {
		defer func() {
			done <- struct{}{}
		}()

		for {
			select {
			case certs, ok := <-certsChan:
				if !ok {
					certsChan = nil
					return
				}

				allCerts = append(allCerts, certs...)
			case err := <-errChan:
				firstErr = err
				return
			}
		}
	}()

	wg.Wait()
	close(certsChan)

	// wait for the results collector
	<-done

	if firstErr != nil {
		return nil, firstErr
	}

	return allCerts, nil
}
