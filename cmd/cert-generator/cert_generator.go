package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"runtime"
	"sync"
	"time"

	"gitlab.com/chenziliang/pkg-go/cert"
	"gitlab.com/chenziliang/pkg-go/http"
	"os"
	"strings"
)

func post(client *http.Client, headers map[string]string, data []byte) error {
	var err error
	for i := 0; i < 10; i++ {
		_, err = client.Request("", "POST", headers, data)
		if err == nil {
			return nil
		}

		if strings.Contains(err.Error(), "ALREADY_EXISTS") {
			return err
		}
		time.Sleep(time.Second)
	}
	return err
}

func add(uri string, headers map[string]string, certs []*cert.Certificate, wg *sync.WaitGroup, id int) {
	defer wg.Done()

	client, err := http.NewClient(uri, 2, 120, false)
	if err != nil {
		return
	}

	fmt.Printf("worker id=%d got %d certs to post\n", id, len(certs))
	for _, cert := range certs {
		m := make(map[string]string, 2)
		m["pem"] = string(cert.Cert)
		data, _ := json.Marshal(m)
		if err := post(client, headers, data); err != nil {
			fmt.Fprintf(os.Stderr, "failed to post certificate, worker id=%d, err=%+v\n", id, err)
		}
	}
	fmt.Printf("worker id=%d finished posting %d certs\n", id, len(certs))
}

func addCertificates(uri string, headers map[string]string, certs []*cert.Certificate) {
	cpus := runtime.NumCPU()
	share := len(certs) / cpus

	var wg sync.WaitGroup
	for i := 0; i < cpus-1; i++ {
		wg.Add(1)
		go add(uri, headers, certs[i*share:(i+1)*share], &wg, i+1)
	}
	wg.Add(1)
	go add(uri, headers, certs[(cpus-1)*share:], &wg, cpus)

	wg.Wait()
}

var (
	host       = flag.String("host", "", "Comma-separated hostnames and IPs to generate a certificate for")
	validFrom  = flag.String("start-date", "", "Creation date formatted as Jan 1 15:04:05 2011")
	validFor   = flag.Duration("duration", 365*24*time.Hour, "Duration that certificate is valid for")
	isCA       = flag.Bool("ca", false, "whether this cert should be its own Certificate Authority")
	rsaBits    = flag.Int("rsa-bits", 2048, "Size of RSA key to generate. Ignored if --ecdsa-curve is set")
	ecdsaCurve = flag.String("ecdsa-curve", "", "ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521")

	numberCerts = flag.Int("number-certs", 10000, "how many number of certs")

	uri   = flag.String("uri", "", "URI to post certificate.")
	token = flag.String("token", "", "token used to post certificate.")
)

func main() {
	flag.Parse()

	if len(*host) == 0 {
		log.Fatalf("Missing required --host parameter")
	}

	if len(*uri) == 0 {
		log.Fatalf("Missing required --uri parameter")
	}

	if len(*token) == 0 {
		log.Fatalf("Missing required --token parameter")
	}

	c := cert.Config{
		Host:       *host,
		ValidFrom:  *validFrom,
		ValidFor:   *validFor,
		IsCA:       *isCA,
		RsaBits:    *rsaBits,
		EcdsaCurve: *ecdsaCurve,
	}

	certs, err := cert.GenerateCerts(&c, *numberCerts)
	if err != nil {
		log.Fatalf("failed to generate certs: %s", err)
	}

	fmt.Printf("done with generating %d certs. try to post to %s\n", len(certs), *uri)

	headers := map[string]string{
		"Content-Type": "application/json",
		"Connection":   "keep-alive",
	}
	headers["Authorization"] = "Bearer " + *token

	addCertificates(*uri, headers, certs)
}
