module gitlab.com/chenziliang/pkg-go

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/sys v0.0.0-20190416152802-12500544f89f
)
