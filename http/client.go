package http

import (
	"bytes"
	"crypto/tls"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type Error struct {
	Code    int
	Message string
}

func (e *Error) Error() string {
	return e.Message
}

type Client struct {
	serverURI string
	client    *http.Client
}

func NewClient(serverURI string, maxKeepAliveConn, timeout int, validateCert bool) (*Client, error) {
	tr := &http.Transport{
		TLSClientConfig:     &tls.Config{InsecureSkipVerify: !validateCert},
		MaxIdleConnsPerHost: maxKeepAliveConn,
	}
	client := &http.Client{Transport: tr, Timeout: time.Duration(timeout) * time.Second}

	return &Client{
		serverURI: serverURI,
		client:    client,
	}, nil
}

func (c *Client) Request(
	path string, method string, headers map[string]string, data []byte) ([]byte, error) {
	var reader io.Reader
	if data != nil {
		reader = bytes.NewBuffer(data)
	}

	return c.doRequest(c.serverURI+path, method, headers, reader)
}

func (c *Client) doRequest(serverURI, method string,
	headers map[string]string, reader io.Reader) ([]byte, error) {
	req, err := http.NewRequest(method, serverURI, reader)
	if err != nil {
		return nil, err
	}

	addHeaders(req, headers)
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	res, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, &Error{resp.StatusCode, err.Error()}
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return nil, &Error{resp.StatusCode, string(res)}
	}
	return res, nil
}

func addHeaders(req *http.Request, headers map[string]string) {
	hasContentType := false
	if headers != nil {
		for k, v := range headers {
			req.Header.Add(k, v)
		}

		if headers["Content-Type"] != "" || headers["content-type"] != "" {
			hasContentType = true
		}
	}

	if !hasContentType {
		req.Header.Add("Content-Type", "application/json")
	}
}
